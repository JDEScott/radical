# read.jl
# Read in particle data from OSIRIS

module Read

using HDF5
using Serialization

include(joinpath(@__DIR__, "types.jl"))
import .Types: Coordinate, Particle

"""Read particle paths from OSIRIS data file."""
function source(filepath::String = joinpath(@__DIR__, "beam-tracks_rep.h5"))
    source = Dict{String, Particle}()
    
    h5open(filepath, "r") do f
        println("Reading data from $filepath...")
        for particle in keys(f)
            q = HDF5.read(f[particle]["q"])[1]
            t = HDF5.read(f[particle]["t"])
            x1 = HDF5.read(f[particle]["x1"])
            x2 = HDF5.read(f[particle]["x2"])
            x3 = zeros(length(x2)) 
            xs = collect(Coordinate(i) for i in zip(x1, x2, x3, t))
            p1 = HDF5.read(f[particle]["p1"])
            p2 = HDF5.read(f[particle]["p2"])
            p3 = HDF5.read(f[particle]["p3"])
            β = collect(i for i in zip(p1, p2, p3))
            source[particle] = Particle(q, xs, β)
        end
    end

    return source
end #function read

"""Read data if saved by Serializaition."""
function serial(filepath::String = joinpath(@__DIR__, "my_file.dat"))
    println("Reading data from $filepath...")
    field = Serialization.deserialize(filepath)
    return field
end #function serial

end #module Read
