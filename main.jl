# main.jl
# Calculate betatron radiation resulting from OSIRIS particle path data

using FFTW

using Distributed
@everywhere include(joinpath(@__DIR__, "calculate.jl"))
@everywhere include(joinpath(@__DIR__, "test.jl"))

include(joinpath(@__DIR__, "field.jl"))
include(joinpath(@__DIR__, "read.jl"))
include(joinpath(@__DIR__, "write.jl"))

function main()
    field, source = Test.rs()

    @time field = Calculate.calculate(field, source)    # Calculate A(t)
    Write.serial(data=field, filepath="field.dat")

    fieldω = Calculate.ft(field)
    spectrum = 2 * Calculate.Maths.absft(fieldω).^2

    # https://stackoverflow.com/questions/56030394/how-to-visualize-fft-of-a-signal-in-julia
    time = reshape(collect(i.x[4] for i in field.xf), length(field.xf)) |> fftshift
    freqs = fftfreq(length(time), 1/(time[2] - time[1])) |> fftshift
   
    data = Dict{String, Any}("time"=>time, "frequency"=>freqs, "spectrum"=>spectrum)
    Write.serial(data=data, filepath="spectrum.dat")
end

if realpath(PROGRAM_FILE) == realpath(@__FILE__) && length(PROGRAM_FILE) != 0
    main()
end
