# test.jl
# Test cases

module Test

include(joinpath(@__DIR__, "field.jl"))

include(joinpath(@__DIR__, "constants.jl"))
import .Constants: c0, ϵ0, e

include(joinpath(@__DIR__, "types.jl"))
import .Types: Coordinate, Particle

"""Persistent static source."""
function pss()
    t_scale = 200π

    # Define field
    x = range(-0.5,     stop=0.5,      length=10_000)
    y = range(0.0,      stop=0.0,       length=1)
    z = range(0.0,      stop=0.0,       length=1)
    t = range(t_scale * 1/c0,    stop=t_scale * 1/c0,     length=1)
    field = Field.define(x=x, y=y, z=z, t=t)
 
    # Define source
    q::Float64 = -e
    x = range(0.0,  stop=0.0,   length=1)
    y = range(0.0,  stop=0.0,   length=1)
    z = range(0.0,  stop=0.0,   length=1)
    t = range(0.0,  stop=t_scale * 1/c0, length=10_000_000)
    xs = collect(Coordinate(i) for i in Iterators.product(x, y, z, t))
    
    β = [[0.0 for _ = 1:3] for _ = 1:length(t)]

    source = [Particle(q, xs, β)]

    return field, source
end


"""Moving source."""
function ms()
    # Define source
    q::Float64 = -e
    β0 = 0.9
    sxmin = 0.0
    sxmax = 200π
    nstep = 10_000_000
    x = range(sxmin,  stop=sxmax, length=nstep)
    y = range(0.0,    stop=0.0,   length=nstep)
    z = range(0.0,    stop=0.0,   length=nstep)
    t = range(0.0,    stop=abs(sxmax - sxmin)/(β0 * c0), length=nstep)
    xs = collect(Coordinate(i) for i in zip(x, y, z, t))

    β = [[β0, 0.0, 0.0] for _ = 1:length(t)]

    source = [Particle(q, xs, β)]

    # Define field
    fxmin = sxmax - 10
    fxmax = sxmax + 10
    x = range(fxmin,    stop=fxmax,     length=10_000)
    y = range(0.0,      stop=0.0,       length=1)
    z = range(0.0,      stop=0.0,       length=1)
    t = range(sxmax/(β0 * c0),    stop=sxmax/(β0 * c0),     length=1)
    field = Field.define(x=x, y=y, z=z, t=t)
 
    return field, source
end

"""Rotating source."""
function rs()
    nstep = 1_000_000
    n_osc = 100

    # Define source
    q::Float64 = -e
    β0 = 0.9
    v = c0 * β0
    ρ = 0.25
    ω = v/ρ

    t = range(0.0,  stop=n_osc*(2π/ω), length=nstep)
    δt = t[2] - t[1]
    x = range(0.0,  stop=0.0,   length=nstep)
    y = zeros(nstep)
    z = zeros(nstep)
    for i = 1:nstep
        # trig functions could be opposite way
        y[i] = x[i] + ρ * sin(ω * t[i])
        z[i] = x[i] + ρ * cos(ω * t[i])
    end
    xs = collect(Coordinate(i) for i in zip(x, y, z, t))

    β = [[0.0, 0.0, 0.9] for _ = 1:nstep]
    for i = 2:nstep
        β[i] = [(x[i] - x[i-1]) / δt, (y[i] - y[i-1]) / δt, (z[i] - z[i-1]) / δt] ./ c0
    end

    source = [Particle(q, xs, β)]

    # Define field
#    x = range(-0.5,     stop=0.5,      length=10_000)
    x = range(0.0,      stop=0.0,       length=1)
#    y = range(1.1*ρ,      stop=1.1*ρ,       length=1)
    y = range(0.0,      stop=0.0,       length=1)
    z = range(0.0,      stop=0.0,       length=1)
    t = range(0.0,      stop=n_osc * 2π/ω,     length=10_000)
    field = Field.define(x=x, y=y, z=z, t=t)
 
    return field, source
end

"""Undulating source."""
function us()
    nstep = 1_000_000
    n_und = 10

    # Define source
    q::Float64 = -e
    β0 = 0.9
    v = c0 * β0
    A = 0.1
    λ = 0.25
    ω = v/λ

    t = range(0.0,  stop=n_osc*(2π/ω), length=nstep)
    δt = t[2] - t[1]
    x = range(0.0,  stop=0.0,   length=nstep)
    y = zeros(nstep)
    z = zeros(nstep)
    for i = 1:nstep
        # trig functions could be opposite way
        y[i] = x[i] + ρ * sin(ω * t[i])
        z[i] = x[i] + ρ * cos(ω * t[i])
    end
    xs = collect(Coordinate(i) for i in zip(x, y, z, t))

    β = [[0.0, 0.0, 0.9] for _ = 1:nstep]
    for i = 2:nstep
        β[i] = [(x[i] - x[i-1]) / δt, (y[i] - y[i-1]) / δt, (z[i] - z[i-1]) / δt] ./ c0
    end

    source = [Particle(q, xs, β)]

    # Define field
#    x = range(-0.5,     stop=0.5,      length=10_000)
    x = range(0.0,      stop=0.0,       length=1)
    y = range(1.1*ρ,      stop=1.1*ρ,       length=1)
    z = range(0.0,      stop=0.0,       length=1)
    t = range(0.0,      stop=n_osc * 2π/ω,     length=10_000)
    field = Field.define(x=x, y=y, z=z, t=t)
 
    return field, source
end



end #module Test
