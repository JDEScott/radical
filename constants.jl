# constants.jl
# Physical constants

module Constants

export c0, ϵ0, m_e, e, ħ

c0  = 299_792_458       # m/s
ϵ0  = 8.854_187_812e-12 # F/m
μ0  = 1.256_637_062e-6  # H/m
e   = 1.602_177_33e-19  # C
m_e = 9.109_381_88e-31  # kg
ħ   = 1.054_571_817e-34 # J⋅s 

end #module Constants
