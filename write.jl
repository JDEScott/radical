# write.jl
# Write data to file

module Write

using HDF5
using Serialization

function hdf5(; filepath::String = joinpath(@__DIR__, "my_file.h5"), data)
    println("Writing data to $filepath...")
    h5open(filepath, "w") do f
        HDF5.write(f, "E", data) 
    end
end

function serial(; filepath::String = joinpath(@__DIR__, "my_file.dat"), data)
    println("Writing data to $filepath...")
    Serialization.serialize(filepath, data)
end

end #module Save
