# field.jl
# Functionality regarding fields

module Field

include(joinpath(@__DIR__, "types.jl"))

"""Return all combinations of {x, y, z, t} values to produce a spacetime grid."""
function creategrid(x::T, y::T, z::T, t::T) where {T <: StepRangeLen}
    return collect(Types.Coordinate(i) for i in Iterators.product(x, y, z, t))
end

"""Define field using grid coordinates and Field struct."""
function define(;   x::T = range(0.0,  stop=0.0,   length=1),
                    y::T = range(0.0,  stop=0.0,   length=1),
                    z::T = range(0.0,  stop=0.0,   length=1),
                    t::T = range(0.0,  stop=0.0,   length=1)) where {T <: StepRangeLen}
    
    grid = creategrid(x, y, z, t)
    field = Types.Field(grid)

    return field
end

end #module Field
