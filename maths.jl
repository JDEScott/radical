# maths.jl
# Export mathematical functions

module Maths

using FFTW
using StaticArrays

"""Calculate normalised acceleration of particle given four-position and
    normalised velocity. First element is assumed zero because unknown prior
    velocity."""
function accel(xs, β)
    @assert length(xs) == length(β) "Unmatched dimensions: t and β"

    t = collect(i.x[4] for i in xs[:])
    
    α::Array{SVector{3, Float64}} = [[0.0 for _ = 1:3] for _ = 1:length(t)]
    for i = 2:length(α)
        if β[i-1] == β[i]
            α[i] = [0.0 for _ = 1:3]
        else
            α[i] = (β[i-1] .- β[i]) ./ (t[i-1] - t[i])
        end
    end

    return α
end

"""Dot product."""
function ⋅(a, b)
    @assert size(a) == size(b) "Dot product: array dimensions do not match!"
    return sum(a .* b)
end

function ⋅(a::T, b::T) where T <: Tuple
    @assert length(a) == length(b) "Dot product: tuple dimensions do not match!"
    return sum(a .* b)
end


"""Define absolute value of a vector."""
function Base.:abs(x)   # TODO generalise to AbstractArray and Tuple types
#function Base.:abs(x::T) where T <: AbstractArray
    return sqrt(x ⋅ x)
end

"""Absolute value of Fourier transform."""
function absft(x)
    tmp = [0.0 for _ = 1:length(x)]
    for i = 1:length(x)
        tmp[i] = abs(abs.(x[i]))
    end
    return tmp
end

"""(3D) Cross product."""
function ×(a::T, b::T) where T <: AbstractArray
    return [a[2]*b[3] - a[3]*b[2],
            a[3]*b[1] - a[1]*b[3],
            a[1]*b[2] - a[2]*b[1]]
end

"""Fourier transform using FFTW library."""
function ft(field)
    datax = reshape(collect(i[1] for i in field.component), length(field.component))
    datay = reshape(collect(i[2] for i in field.component), length(field.component))
    dataz = reshape(collect(i[3] for i in field.component), length(field.component))
    transf = collect(zip(fft(datax), fft(datay), fft(dataz)))
    return transf
end

end #module Maths
