# types.jl
# Define and export types (structs)

module Types

include(joinpath(@__DIR__, "maths.jl"))
using StaticArrays

export Coordinate, Field, Particle

"""Four-dimensional spacetime coordinates (x, y, z, t)."""
struct Coordinate
    x::SVector{4, Float64}
    Coordinate() = new(zeros(4))
    Coordinate(x) = new(x)
end

"""Fields have components at each coordinate in fieldset."""
struct Field
    xf::Array{Coordinate}
    component::Array{SVector{3, Float64}}
    Field() = new([Coordinate()], [[0.0 for _ = 1:3]])
    Field(grid) = new(grid, reshape([[0.0 for _ = 1:3] for _ = 1:length(grid)], size(grid)))
    Field(a, b) = new(a, b)
end

"""Struct for relevant properties for particles."""
struct Particle
    q::Float64
    xs::Array{Coordinate}
    β::Array{SVector{3, Float64}}   # normalised velocity
    α::Array{SVector{3, Float64}}   # normalised acceleration
    Particle() = new(0.0, [Coordinate()], [[0.0 for _ = 1:3]], [[0.0 for _ = 1:3]])
    Particle(q, xs, β) = new(q, xs, β, Maths.accel(xs, β))
    Particle(q, xs, β, α) = new(q, xs, β, α)
end

end #module Types
