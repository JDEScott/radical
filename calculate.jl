# calculate.jl
# Iterate over all field-source point pairings, summing contributions.

module Calculate

using Distributed

include(joinpath(@__DIR__, "constants.jl"))
import .Constants: c0, ϵ0, m_e, e, ħ

include(joinpath(@__DIR__, "maths.jl"))
import .Maths: ×, ⋅

"""Iterate over all field-source pairings, summing field contributions from all points along all paths of all particles."""
function calculate(field, source)
    println("Starting calculation on $(nprocs()) processes.")

    # TODO agnostic time-/space-dependent element
    δx = c0 * abs(field.xf[2].x[4] -  field.xf[1].x[4])
    δy = δx
    δz = δx
    δct = δx

#    δx = abs(source[1].xs[2].x[1] - source[1].xs[1].x[1])
#    δy = abs(source[1].xs[2].x[2] - source[1].xs[1].x[2])
#    δz = abs(source[1].xs[2].x[3] - source[1].xs[1].x[3])
#    δct = δx

    Δ = √(δx^2 + δy^2 + δz^2)
   
    fp = eachindex(field.xf)
    field.component[fp] = map(+, pmap(calculatefieldpoint, Iterators.product(field.xf[fp], source[:], Δ))) # iterate all fieldpoint-particle pairings

#    for fp in eachindex(field.xf)   # iterate over field points
#        fp % 10 == 0 && println("$fp / $(length(field.xf))")
#        for p in source             # iterate over particles
#            for sp in eachindex(p.xs)   # iterate over source points (path)
#                field.component[fp] += contribution(field.xf[fp], p.xs[sp], p.q, p.β[sp], p.α[sp], Δ)
#            end
#        end
#    end

    return field
end

"""Calculate field strength at given fieldpoint by summing contributions from all particles."""
# TODO replace loop with map -> move into earlier parallel map.
function calculatefieldpoint((fp, p, Δ))
#    sp = eachindex(p.xs)
#    strength = map(+, map(particle, Iterators.product(Iterators.repeated(fp, 1), p.q, Iterators.zip(p.xs, p.β, p.α), Δ)))

    strength = [0.0 for _ = 1:3]

    for sp in eachindex(p.xs)   # iterate over source points (path)
        strength += point(fp, p.q, p.xs[sp], p.β[sp], p.α[sp], Δ)
    end

    return strength
end

"""Calculate field strength at given fieldpoint due to a single sourcepoint."""
function point(fp, q, sp, β, α, Δ)
    xf = fp.x[1:3]
    tf = fp.x[4]
        
    xs = sp.x[1:3]
    ts = sp.x[4]
            
    Δx = xf - xs
    X = abs(xf - xs)
    T = tf - ts
    ds = abs(c0*T - X)

    if ds ≤ Δ
        scale = abs(ds/Δ - 1.0)
        n = Δx ./ X
        γ = 1 / sqrt(1 - β⋅β)

        # TODO make if-else for field/potential        
#        coefficient = q/(4*π*ϵ0)
#        accel = (n × ((n - β) × α)) ./ (c0 * (1 .- (n ⋅ β))^3 * X)
        coefficient = √(c0 / 4π) * q/(4*π*ϵ0)

        accel = (n × ((n - β) × α)) ./ (c0 * (1 .- (n ⋅ β))^3)
        return scale * coefficient * accel
    else
        return [0.0 for _ = 1:3]
    end
end

function ft(field)
    return Maths.ft(field)
end

end #module Calculate
